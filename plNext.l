/************************
* FILE: plnext.l
* AUTHOR: Theo Mok -18808742
* LAST MOD: 12/10/2018
************************/
%{
#include <stdlib.h>
#include "plNext.tab.h"

/*Must use MACRO for text replacement instead of function as the token
  needs to be returned to yacc not returned from the called function*/
#define TOKEN_FOUND(TOKEN){printf("LEX FOUND TOKEN %s\n",yytext);return TOKEN;}

#define IDENT_FOUND(TOKEN){printf("LEX FOUND IDENT %s\n",yytext);return TOKEN;}
#define NUMBER_FOUND(TOKEN){printf("LEX FOUND NUMBER %s\n",yytext);return TOKEN;}

%}
%%

PROGRAM     TOKEN_FOUND(PROGRAM)

TERMINATE   TOKEN_FOUND(TERMINATE)

DECL        TOKEN_FOUND(DECL)

DECLARATION TOKEN_FOUND(DECLARATION)

END         TOKEN_FOUND(END)

CONST       TOKEN_FOUND(CONST)

VAR         TOKEN_FOUND(VAR)

PROC        TOKEN_FOUND(PROC)

FUNC        TOKEN_FOUND(FUNC)

TYPE        TOKEN_FOUND(TYPE)

IS          TOKEN_FOUND(IS)

ARR         TOKEN_FOUND(ARR)

OF          TOKEN_FOUND(OF)

TO          TOKEN_FOUND(TO)

IMPL        TOKEN_FOUND(IMPL)

SET         TOKEN_FOUND(SET)

EXECUTE     TOKEN_FOUND(EXECUTE)

IF          TOKEN_FOUND(IF)

THEN        TOKEN_FOUND(THEN)

FI          TOKEN_FOUND(FI)

WHILE       TOKEN_FOUND(WHILE)

DO          TOKEN_FOUND(DO)

ELIHW       TOKEN_FOUND(ELIHW)

OD          TOKEN_FOUND(OD)

FOR         TOKEN_FOUND(FOR)

ROF         TOKEN_FOUND(ROF)

START       TOKEN_FOUND(START)

STOP        TOKEN_FOUND(STOP)

":"         TOKEN_FOUND(':')
";"         TOKEN_FOUND(';')
","         TOKEN_FOUND(',')
"."	    TOKEN_FOUND('.')

"="         TOKEN_FOUND('=')
">"         TOKEN_FOUND('>')

"("         TOKEN_FOUND( '(' )
")"         TOKEN_FOUND( ')' )

"{"         TOKEN_FOUND('{')
"}"         TOKEN_FOUND('}')

"["         TOKEN_FOUND('[')
"]"         TOKEN_FOUND(']')

"+"         TOKEN_FOUND('+')
"-"         TOKEN_FOUND('-')
"*"         TOKEN_FOUND('*')
"/"         TOKEN_FOUND('/')




[0-9]+      NUMBER_FOUND(number); //dosent allow for neagtive numbers

[a-z]+      IDENT_FOUND(ident); //lowercase only idents,no numbers?

[ \n\t\r]+  //do nothing, igore whitespace      

.           {
		//print error and exit the program
              fprintf(stderr,"LEX ERROR unexpected token: '%s' ",yytext);
              exit(1);
            }

%%



int yywrap(void)
{
    return 1;
}

EXE = PL-NEXT-SYNTAX
FLEX = flex
BISON = bison
LEXFILE = plNext.l
YACCFILE = plNext.y
CFILES = lex.yy.c plNext.tab.c
HEADERFILES = plNext.tab.h
OUTPUT = out.txt
FLEXFLAG = -lfl
CC = gcc


$(EXE): $(CFILES)
	$(CC) -o $(EXE) $(CFILES) $(FLEXFLAG)

lex.yy.c : $(LEXFILE) y.tab.c
	$(FLEX) $(LEXFILE)

y.tab.c: $(YACCFILE)
	$(BISON) -d $(YACCFILE)

clean:
	rm -f $(EXE) $(CFILES) $(HEADERFILES) $(OUTPUT) 

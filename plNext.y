/***********************
* FILE: plnext.y
* AUTHOR: Theo Mok 18808742
* LAST MOD: 12/10/2018
***********************/
%{
    #include <stdio.h>

    
    void grammar_found(char *type)
    {
	printf("Yacc found %s\n", type);
    }


    #define YYERROR_VERBOSE
%}

%token  PROGRAM
        TERMINATE
        DECL
        DECLARATION
        END
        CONST
        VAR
        PROC
        FUNC
        TYPE
        IS
        ARR
        OF
        TO
        IMPL
        SET
        EXECUTE
        IF
        THEN
        FI
        WHILE
        DO
        ELIHW
        OD
        FOR
        ROF
        START
        STOP
        number
        ident


%%

basic_program:
    PROGRAM declaration_unit implementation_unit TERMINATE 
    {grammar_found("basic_program");};


/*optional Declaration Unit parts*/

optional_constant_declaration:
    CONST constant_declaration      //can have constant_declaration
    {grammar_found("optional_constant_declaration");} //only call when actually found 
    |;                            //or nothing, no function call if its empty

optional_variable_declaration:
    VAR variable_declaration
    {grammar_found("optional_variable_declaration");}
    |;

optional_type_declaration:
    type_declaration
    {grammar_found("optional_type_declaration");}
    |;

optional_procedure_interface:
    procdure_interface
    {grammar_found("optional_procedure_interface");}
    |;

optional_function_interface:
    function_interface
    {grammar_found("optional_function_interface");}
    |;


/*declaration_unit section*/

declaration_unit:                   //full form of declaration_unit
    DECL':'':'ident
    optional_constant_declaration   
    optional_variable_declaration   //onyl allows for one prodecure?
    optional_type_declaration
    optional_procedure_interface
    optional_function_interface
    DECLARATION END
    {grammar_found("declaration_unit");};








optional_formal_paramaters:
    formal_parameters               //formal_parameters
    {grammar_found("optional_formal_paramaters");}
    |;                            //or nothing, no call if its empty


procdure_interface:
    PROC ident                      //must have this
    optional_formal_paramaters     //can have formal paramaters|nothing
    {grammar_found("procdure_interface");};

function_interface:
    FUNC ident
    optional_formal_paramaters
    {grammar_found("function_interface");};




type_declaration:
    TYPE ident '=' '>' type ';'
    {grammar_found("type_declaration");};

ident_and_semicolon_loop:
    ident
    | ident_and_semicolon_loop ';' ident
    {grammar_found("ident_and_semicolon_loop");};                    

formal_parameters:
    '(' ident_and_semicolon_loop ')'
    {grammar_found("formal_parameters");};                        




constant_declaration_loop:
    ident IS number
    | constant_declaration_loop ',' ident IS number
    {grammar_found("constant_declaration_loop");};           

constant_declaration:
    constant_declaration_loop ';'                           //any number of constants then semicolon                                                       
    {grammar_found("constant_declaration");};




variable_declaration_loop:
    ident ':' ident
    |variable_declaration_loop ',' ident ':' ident          
    {grammar_found("variable_declaration_loop");};

variable_declaration:
    variable_declaration_loop ';'                           
    {grammar_found("variable_declaration");};






type:
    basic_type
    {grammar_found("type");}
    | array_type
    {grammar_found("type");};

basic_type:
    ident
    {grammar_found("basic_type");}
    | enumerated_type
    {grammar_found("basic_type");}
    | range_type
    {grammar_found("basic_type");};

comma_ident_loop:
    ident
    | comma_ident_loop ',' ident
    {grammar_found("comma_ident_loop");};

enumerated_type:
    '{' comma_ident_loop '}'
    {grammar_found("enumerated_type");};

range_type:
    '[' range ']'
    {grammar_found("range_type");};

array_type:
    ARR ident '[' range ']' OF type
    {grammar_found("array_type");};

range:
    number TO number
    {grammar_found("range");};


/*Imeplementation unit section*/


implementation_unit:
    IMPL ':'':'ident block '.'
    {grammar_found("implementation_unit");};

block:
    specification_part implementation_part
    {grammar_found("block");};

specification_part:
    CONST constant_declaration
    {grammar_found("specification_part");}
    | VAR variable_declaration
    {grammar_found("specification_part");}
    | procedure_declaration
    {grammar_found("specification_part");}
    | function_declaration
    {grammar_found("specification_part");}
    | {}; //no call if its empty

procedure_declaration:
    PROC ident ';' block ';'
    {grammar_found("procedure_declaration");};

function_declaration:
    FUNC ident ';' block ';'
    {grammar_found("function_declaration");};

implementation_part:
    statement
    {grammar_found("implementation_part");};

statement:
    assignment
    {grammar_found("statement");}
    | procedure_call
    {grammar_found("statement");}
    | if_statement
    {grammar_found("statement");}
    | while_statement
    {grammar_found("statement");}
    | do_statement
    {grammar_found("statement");}
    | for_statement
    {grammar_found("statement");}
    | compound_statement
    {grammar_found("statement");};

assignment:
    ident SET expression
    {grammar_found("assignment");};

procedure_call:
    EXECUTE ident
    {grammar_found("procedure_call");};

if_statement:
    IF expression THEN statement FI
    {grammar_found("if_statement");};

semicolon_statement_loop:
    statement
    | semicolon_statement_loop ';' statement
    {grammar_found("semicolon_statement_loop");};

while_statement:
    WHILE expression DO semicolon_statement_loop ELIHW
    {grammar_found("while_statement");};

do_statement:
    DO semicolon_statement_loop WHILE expression OD
    {grammar_found("do_statement");};

for_statement:
    FOR ident ':''=' expression DO semicolon_statement_loop ROF
    {grammar_found("for_statement");};

compound_statement:
    START semicolon_statement_loop STOP
    {grammar_found("compound_statement");};

/*Expressions section*/

expression_loop:
    term                            //at least one term by itself, no call as its not a loop
    | expression_loop '+' term      // any number of + - terms
    {grammar_found("expression_loop");}
    | expression_loop '-' term
    {grammar_found("expression_loop");};                   

expression:
    expression_loop
    {grammar_found("expression");};

term_loop:
    id_num  //no call as its not a loop
    | term_loop '*' id_num
    {grammar_found("term_loop");}
    | term_loop '/' id_num
    {grammar_found("statement");};

term:
    term_loop
    {grammar_found("term");};

id_num:
    ident
    {grammar_found("id_num");}
    |number
    {grammar_found("id_num");};

//ident and number are defiend in lex

%%
int main()
{
    //starts the yacc parser
    return yyparse();
}
int yyerror(char *s)
{
    fprintf(stderr, "%s\n",s);
}
